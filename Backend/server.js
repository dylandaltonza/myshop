import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import bodyParser from 'body-parser';
import Product from './models/product';

const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/products',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB Database connection established successfully!');
});

router.route('/home').get( (req, res) => {
    Product.find( (err, products) => {
        if(err){
            console.log(err);
        }
        else {
            res.json(products);
        }
    })
});

app.use('/', router);

app.listen(4000, () => {
    console.log('Express server running on port 4000');
});
