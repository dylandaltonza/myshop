import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Product = new Schema({
    id: {
        type: Number
    },
    name: {
        type: String
    },
    description: {
        type: String
    },
    price: {
        type: Number
    },
    imageUrl: {
        type: String
    }
});

export default mongoose.model('Product', Product);