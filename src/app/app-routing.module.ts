import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './components/home-page/home-page.component';
import { SearchComponent } from './components/search/search.component';
import { BasketComponent } from './components/basket/basket.component';

const routes: Routes = [
  {path: 'home', component: HomePageComponent },
  {path: 'search', component: SearchComponent },
  {path: 'basket', component: BasketComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

 export const routingComponents = [
  HomePageComponent,
  SearchComponent,
  BasketComponent
 ];